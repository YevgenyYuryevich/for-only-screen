import {Directive, Input, TemplateRef, ViewContainerRef, ElementRef, OnInit, OnDestroy} from '@angular/core';
import breakpoints from '../config/breakpoints';
@Directive({
  selector: '[appForOnlyScreen]'
})
export class ForOnlyScreenDirective implements OnInit, OnDestroy {
  private hasView = false;
  private viewportWidth = window.innerWidth;
  private _forOnlyScreen: string;
  @Input() set appForOnlyScreen(screen: string) {
    this._forOnlyScreen = screen;
    this.renderWith(screen);
  }
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef, private el: ElementRef) {
  }
  private renderWith(screen) {
    const condition = this.canShow(screen);
    if (condition && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (!condition && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
  private canShow(screen: string): boolean {
    return this.currentScreen === screen;
  }
  get currentScreen() {
    if (this.viewportWidth < breakpoints.mobile) {
      return 'mobile';
    } else if (this.viewportWidth < breakpoints.tablet) {
      return 'tablet';
    } else {
      return 'desktop';
    }
  }
  private resizeListener() {
    this.viewportWidth = window.innerWidth;
    this.renderWith(this._forOnlyScreen);
  }
  ngOnInit() {
    window.addEventListener('resize', this.resizeListener.bind(this));
  }
  ngOnDestroy(): void {
    window.removeEventListener('resize', this.resizeListener);
  }
}
